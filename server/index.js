// IMPORTS FROM PACKAGES
const express = require('express');
const mongoose = require('mongoose');
// IMPORTS FROM OTHER FILES
const authRouter = require('./routes/auth');


// INIT

const PORT = 3000;
const app = express();
const DB = 'mongodb+srv://Almin:JakaSifra123@cluster0.y9lvtxq.mongodb.net/?retryWrites=true&w=majority';

// middleware
// client -> middleware -> server -> client
app.use(express.json());
app.use(authRouter);

// connections
mongoose
  .connect(DB)
  .then(() => {
    console.log('Connected to MongoDB');
  })
  .catch((e) => {
    console.log(e);
  });


// creating an API
// GET, PUT, POST, DELETE, UPDATE -> CRUD (Create, Read, Update, Delete)
// http://<youtIpAddress>/hello-world
// app.get('/hello-world', (req, res) => {
//   res.json({hi: 'Hello World!'});

// });


app.listen(PORT, "0.0.0.0", () => {
  console.log(`connected at port ${PORT}`);
});